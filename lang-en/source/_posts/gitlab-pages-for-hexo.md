---
title: GitLab Pages For Hexo
date: 2017-10-11
categories: [Web]
tags: [Hexo,GitLab-CI]
---


### Get Started

1. Click [example projects for gitlab pages](https://gitlab.com/groups/pages)
2. Fork the project of hexo to your account(If you do not have any gitlab account, you can create a new)
3. Remove fork relationship(Choose settings->General from the list on the left.And expand Advanced settings to remove fork relationship)
4. Optional: modify project info，such as Project name, Project description, Path etc. *Here's a suggestion about that path and Project name modify together.* your website will be available at https://username.gitlab.io/projectname, it is familiar with gitlab pages.
5. Optional: **suggested modify the node's version from `.gitlab-ci.yml` to 6.11.2**
6. Modify any file, such as readme.md or the article under source/_posts
7. Choose CI/CD, waiting job completion
8. Click https://your-name.gitlab.io/projext-name

![](https://jiangtj.github.io/assets/img/others/ci-1.jpg)  

![](https://jiangtj.github.io/assets/img/others/ci-2.jpg)  

### Advanced

- you can modify script in `.gitlab-ci.yml`.
- you can change the theme to next, which is most popular theme of hexo, by a official tutorial <http://theme-next.iissnan.com>
